/* globals require*/

    "use strict";
var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    rename = require('gulp-rename'),
    poststylus = require('poststylus'),
    autoprefixer = require('autoprefixer'),
    inject = require('gulp-inject-string'),
    browserify = require('browserify'),
    uglify = require('gulp-uglifyjs'),
    plumber = require('gulp-plumber'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    sourcemaps  = require('gulp-sourcemaps'),
    gutil       = require('gulp-util'),
    $ = require('gulp-load-plugins')(),
    del = require('del'),
    watchify = require('watchify'),
    stylish = require('jshint-stylish'),
    i18n = require('browserifyi18n'),
    _ = require('lodash'),
    webserver = require('gulp-webserver');

gulp.task('clean-js', function(cb) {
    return del([
        'public/js/**'
    ], cb);
});

gulp.task('clean-css', function(cb) {
    return del([
        'public/css/**'
    ], cb);
});

gulp.task('build-css', function(){
    gulp.src('./src/stylus/*.styl')
        .pipe(plumber())
        .pipe(stylus({
            compress: true,
            use: [
                poststylus(['autoprefixer'])
            ]
        }))
        .pipe(rename('main.css'))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('build-js', function() {
    var b = browserify({
        entries: './src/js/main.js',
        debug: true,
        insertGlobals : true,
        paths: [
            './src/js',
            './src/js/application',
            './src/js/form',
            './src/js/loader',
            './src/js/login',
            './src/js/users'
        ]
    });

    return b.bundle()
        .pipe(source('main.js'))
        .pipe(plumber())
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
            //.pipe(uglify())
            .on('error', gutil.log)
        .pipe(sourcemaps.write())
        .pipe(rename('main.js'))
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('build', function(){
    gulp.start('build-css', 'build-js');
});

gulp.task('watch', function(){
   gulp.watch('./src/stylus/**/*.styl', ['build-css']);
   gulp.watch(['./src/js/**/*.js', './src/js/**/*.hbs'], ['build-js']);
});

gulp.task('server', function() {
  gulp.src('public')
      .pipe(webserver({
        livereload: true,
        directoryListing: true,
        open: true,
        fallback: 'index.html',
        directoryListing: {
          enable:false,
          path: 'public'
        }
      }));
});
/*

gulp.task('html', function() {
    return gulp.src('./src/index.html')
        .pipe($.plumber())
        .pipe(gulp.dest('./dist'));
});

var bundler = _.memoize(function(watch) {
    var options = {debug: true};

    if (watch) {
        _.extend(options, watchify.args);
    }

    var b = browserify('./src/main.js', options);

    if (watch) {
        b = watchify(b);
    }

    return b;
});

var handleErrors = function() {
    var args = Array.prototype.slice.call(arguments);
    delete args[0].stream;
    $.util.log.apply(null, args);
    this.emit('end');
};

function bundle(cb, watch) {
    return bundler(watch).bundle()
        .on('error', handleErrors)
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe($.sourcemaps.init({ loadMaps: true }))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./dist'))
        .on('end', cb)
}

gulp.task('scripts', function(cb) {
    bundle(cb, true);
});

gulp.task('jshint', function() {
    return gulp.src(['./src/!**!/!*.js', './test/!**!/!*.js'])
        .pipe($.plumber())
        .pipe($.jshint())
        .pipe($.jshint.reporter(stylish));
});

var reporter = 'spec';

gulp.task('mocha', ['jshint'], function() {
    return gulp.src([
            './test/setup/node.js',
            './test/setup/helpers.js',
            './test/unit/!**!/!*.js'
        ], { read: false })
        .pipe($.plumber())
        .pipe($.mocha({ reporter: reporter }));
});

gulp.task('build', [
    'clean',
    'html',
    'styles',
    'scripts',
    'test'
]);


gulp.task('watch', ['build'], function() {
    gulp.watch('./test/!**!/!*.js', ['test']);
    gulp.watch(['./src/main.less', './src/!**!/!*.less'], ['styles']);
    gulp.watch(['./src/!*.html'], ['html']);
});*/

gulp.task('default', ['build', 'watch', 'server']);