import Backbone from 'backbone';
import Marionette from 'backbone.marionette';
import Vent from './vent';
import MainModel from './mainModel';

export default Marionette.AppRouter.extend({
    routes: {
      'shop': 'shoppingForm',
      'users': 'userSelect',
      '': 'init'
    },

    onRoute() {
      "use strict";
      console.log('onRoute', arguments);
    },

    init() {
        "use strict";
        Vent.vent.trigger('load:page:init');
    },

    shoppingForm() {
      "use strict";
      console.log('formRoute');
      if(MainModel.isUser()) {
        Vent.vent.trigger('load:page:shoppingForm');
      } else {
        this.navigate('users', {trigger: true});
      }
    },

    userSelect(){
      "use strict";
      Vent.vent.trigger('load:page:users');
    }
});