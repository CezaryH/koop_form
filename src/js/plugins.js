import Backbone from 'backbone';
import $ from 'jquery';
import Marionette from 'backbone.marionette';
import Handlebars from 'hbsfy/runtime';
import 'babel-polyfill';
import _ from  'underscore';

Backbone.$ = $;

Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
    if (arguments.length < 3)
        throw new Error("Handlebars Helper equal needs 2 parameters");
    if( lvalue!=rvalue ) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    }
});

if (window.__agent) {
    window.__agent.start(Backbone, Marionette);
}

export {_, $, Backbone, Marionette};