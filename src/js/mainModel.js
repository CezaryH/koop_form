import {Model} from 'backbone';

let MainModel =  Model.extend({
  active_user: false,
  isUser() {
    return !!this.get('active_user');
  }
});

export default new MainModel();