"use strict";
import $ from 'jquery';
import config from 'config';
$.ajaxSettings.traditional = true;

let executeRequest = function(func, serializedData) {
    if (!func){
        throw new Error('no function to call');
    }

    let $dfd = $.Deferred();

    $.ajax({
        crossDomain: true,
        url: `${config.get('Client.google_docs_url')}?callback=?`,
        timeout: 15000,
        method: "GET",
        data: {
            method: func,
            data: $.param(serializedData || [])
        },
        dataType: "jsonp",
        contentType: "text/json",
        mimeType: "text/javascript",
        success: function(response, status, xhr) {
            if(response.data && response.status == 200) {
                $dfd.resolve(response.data);
            } else {
                $dfd.reject();
                console.error('connection error - show message', response, status);
            }
        },
        error: function(xht, t, m) {
            $dfd.reject();
            console.error('connection error - show message', t, m);
        }
    });

    return $dfd.promise();
};

export default {
    setData: function(data) {
        return executeRequest('setData', data);
    },
    getData: function() {
        return executeRequest('getData');
    },

    getGroceries: function(data) {
        return executeRequest('getGroceries', data);
    },

    getGroceriesCurrentOrder: function (data) {
        return executeRequest('getGroceriesCurrentOrder', data);
    },

    getProviders: function() {
        return executeRequest('getProviders');
    },

    getUsers: function() {
        return executeRequest('getUsers');
    },

    getFull: function() {
        return executeRequest('getFull');
    }
};