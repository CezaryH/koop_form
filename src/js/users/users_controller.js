import gApi from 'google.client';
import {$} from 'plugins';
import Vent from './../vent';
import UsersCollection from './collection/users_collection';
import UsersView from './view/users_view';

function init() {
    "use strict";

    $.when(gApi.getUsers()).done(users => {
      Vent.commands.execute('load:view', new UsersView({
        collection: new UsersCollection(users)
      }));
    }).fail(() => {
      Vent.commands.execute('load:page:error', 'USERS_CONNECTION_ERROR');
    });
}

Vent.vent.on('load:page:users', init);