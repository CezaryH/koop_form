import {ItemView} from 'backbone.marionette';
import UsersTemplate from './../templates/users_template.hbs';
import MainModel from '../../mainModel';

export default ItemView.extend({
    template: UsersTemplate,
    ui: {
        'select': 'select'
    },
    events: {
        'change @ui.select': 'selectUser'
    },
    selectUser: function(e){
        "use strict";
        MainModel.set('active_user', e.target.value);
    }
});