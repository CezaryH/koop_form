import _ from 'underscore';
import {LayoutView} from 'backbone.marionette';
import Vent from './../vent';
import LayoutTemplate from './layout-template.hbs';
import menuView from '../ententies/view/menu_view';

export default LayoutView.extend({
    el: '.application',
    template: LayoutTemplate,
    regions: {
        menu : '.js_application__menu',
        content : '.js_application__content',
        message: '.js_application__message'
    },

    initialize() {
        "use strict";
        Vent.commands.setHandler('load:view', _.bind(this.renderContent, this));
        Vent.commands.setHandler('load:view:message', _.bind(this.showMessage, this));

    },

    onRender(){
      this.getRegion('menu').show( new menuView());
    },

    showMessage(view) {
        "use strict";
        this.getRegion('message').show(view);
    },

    renderContent (view) {
        "use strict";
        this.getRegion('content').show(view);
    }
});