import {Application} from 'backbone.marionette';
import LayoutView from './layout-view';

let app = Application.extend({
    initialize() {
        "use strict";
        this.layout = new LayoutView();
        this.layout.render();
    }
});

let App = new app();

export default App;