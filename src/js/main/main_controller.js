import Vent from './../vent';
import menuTemplate from './../ententies/template/menu_template.hbs';
import MenuView from './../ententies/view/menu_view';
import {ItemView} from 'backbone.marionette';

let init = function () {
    "use strict";
    Vent.commands.execute('load:view', new MenuView({
        template: menuTemplate
    }));
};

Vent.vent.on('load:page:init', init);