import { ItemView } from 'backbone.marionette';
import Application from '../../application/application';
import menuTemplate from '../template/menu_template.hbs';
export default ItemView.extend({
    tagName: 'ul',
    template: menuTemplate,
    ui: {
        'link' : 'a'
    },

    events: {
        'click @ui.link': 'linkClick'
    },

    initialize() {
        "use strict";
        console.log(this);
    },

    linkClick (e) {
        "use strict";
        e.preventDefault();
        e.stopPropagation();
        Application.Router.navigate(e.target.pathname, {trigger: true});
    }
});