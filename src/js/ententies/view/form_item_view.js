import _ from 'underscore';
import {ItemView} from 'backbone.marionette';
import FormItemTemplate from './../template/form_item_template.hbs';


export default ItemView.extend({
    template: FormItemTemplate,
    tagName: "tr",
    className: function(){
        "use strict";
        var className = [];
        if(this.model.get('is_blocked')){
            className.push('blocked');
        }

        return className.join(' ');
    },
    ui: {
        'select': 'select'
    },
    events: {
        'change @ui.select': 'valueChanged'
    },
    modelEvents: {
        'change': 'render'
    },
    initialize: function(options) {
        "use strict";
    },
    onRender: function() {
        "use strict";
        this.$el.attr('class', this.className());
    },
    templateHelpers: function() {
        "use strict";
        let ret = {
            order_max: () => {
                return (this.model.get('order_max') !== Infinity) ? this.model.get('order_max') : '';
            },
            avail_weights: () => {
                return _.filter(this.model.get('avail_weights'), w => w <= this.model.get('order_max') );
            }
        };

        _.each(this.options.cols, function(col, index){
           ret['hasCol_' + index] = true;
        });

        return ret;
    },
    valueChanged: function(e) {
        "use strict";
        this.model.set('value', parseInt(e.target.value,10));
    }
});