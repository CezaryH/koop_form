import {CompositeView} from 'backbone.marionette';
import FormTemplate from './../template/form_template.hbs';
import FormItemView from './form_item_view';


export default CompositeView.extend({
    template: FormTemplate,
    childViewContainer: "tbody",
    childView: FormItemView
});