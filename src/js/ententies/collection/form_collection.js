import {Collection} from 'backbone';
import _ from 'underscore';
import model from '../model/form_item_model';
import gApi from 'google.client';
import $ from 'jquery';
import MainModel from 'mainModel'

const CHECK_STORE_INTERVAL = 5000;
let getData = function(call, doneCb, failCb) {
    "use strict";
    return $.when(call).fail((e) => {
        console.error(e);
    });
};

export default Collection.extend({
    model: model,
    initialize: function() {
        "use strict";
        this.getGroceries();
    },

    getGroceries: function() {
        "use strict";
        getData(gApi.getGroceries({column: MainModel.get('active_user')})).done(groceries => {
            this.set(groceries);
            setInterval( () => getData( gApi.getGroceriesCurrentOrder() ).done( groceries => {
                this.set(groceries, {remove: false});
              }), CHECK_STORE_INTERVAL);
        });
    }
});