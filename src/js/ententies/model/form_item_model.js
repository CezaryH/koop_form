import _ from 'underscore';
import {Model} from 'backbone';
import gApi from '../../google.client';
import mainModel from '../../mainModel';

export default Model.extend({
    defaults: {
      total_price: 0,
      value:0,
      is_blocked: false
    },
    parse: function(attrs, options) {
      "use strict";
      if(attrs.value > 0){
          attrs.total_price = parseFloat(attrs.price * attrs.value).toFixed(2);
      }

      if(attrs.value >= attrs.order_max) {
          attrs.is_blocked = true;
      }
      return attrs;
    },
    initialize: function(){
      "use strict";
      this.on('change:value', () => {
        this.saveValue();
        this.updateTotal();
        this.checkMax();
      });
    },
    saveValue: function(){
        "use strict";
        gApi.setData({
            row: this.get('id'),
            column: parseInt(mainModel.get('active_user'),10),
            value: this.get('value')
        }, _.bind(this.onAfterSave, this));
    },
    updateTotal: function() {
        "use strict";
        this.set('total_price', parseFloat(this.get('value') * this.get('price')).toFixed(2));
    },
    checkMax: function() {
        "use strict";
        this.set('is_blocked', this.get('value') >= this.get('order_max'));
    },
    onAfterSave: function() {
        "use strict";
        console.log(' onAfterSave', this);
    }
});