import _ from 'underscore';
import {Collection} from 'backbone';
import {LayoutView} from 'backbone.marionette';
import LayoutTemplate from './../template/form_layout.hbs';
import FormView from '../view/form_view';


export default LayoutView.extend({
    template: LayoutTemplate,

    regions: {
        main : '.js_form_main',
        aside: '.js_form_aside'
    },

    initialize() {
        "use strict";
    },

    onRender() {
        this.getRegion('main').show(new FormView({
            cols: {
                provider: 'Dostawca',
                name: 'Nazwa',
                description: 'Opis',
                price: 'Cena',
                order_max: 'Max',
                avail_weights: 'ilość',
                total_price: 'cena całkowita'
            },
            collection: this.options.groceriesCollection
        }));
    }
});