import Backbone from 'backbone';
import './plugins';
import Router from './router';
import Application from './application/application';

import './main/main_controller';
import './error_handler';
import './shopping_form/shoppingForm_controller';
import './users/users_controller';

Backbone.emulateHTTP = true;
Application.Router = new Router();
Backbone.history.start({
    pushState: true
});

Application.start();
