import $ from "jquery";
import gApi from "../google.client";
import Vent from "./../vent";
import MainModel from "./../mainModel";
import GroceriesCollection from "ententies/collection/form_collection";
import FormLayout from "ententies/layout/form_layout";

function init() {
    "use strict";
    $.when(gApi.getGroceries({column: MainModel.get("active_user")})).done(data => {
      Vent.commands.execute("load:view", new FormLayout({
        groceriesCollection: new GroceriesCollection(data, {parse: true})
      }));
    }).fail(() => {
        Vent.commands.execute("load:page:error", "SHOPPING_FORM_CONNECTION_ERROR");
    });
}

Vent.vent.on("load:page:shoppingForm", init);