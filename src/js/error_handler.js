import Vent from './vent';
import {ItemView} from 'backbone.marionette';
import Handlebars from 'handlebars';

let init = function (data) {
    "use strict";
    let View = ItemView.extend({
            className: 'application__message',
            template: Handlebars.compile('error occured : ' + data)
        });
    let mainView = new View();
    Vent.commands.execute('load:view:message', mainView);
};

Vent.commands.setHandler('load:page:error', init);